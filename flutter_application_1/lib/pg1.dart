import "package:flutter/material.dart";

class First extends StatefulWidget {
  const First({super.key});

  State<First> createState() => firstState();
}

class firstState extends State<First> {
  TextEditingController usernametexteditingcontroller = TextEditingController();
  TextEditingController passwordtexteditingcontroller = TextEditingController();

  GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromARGB(255, 97, 95, 207),
        title: const Text(
          "Login_Page",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(30))),
      ),
      body: Padding(
          padding: const EdgeInsets.all(8),
          child: Form(
            key: _formkey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20,
                ),
                Image.network(
                  "https://i.pinimg.com/474x/0a/a8/58/0aa8581c2cb0aa948d63ce3ddad90c81.jpg",
                  height: 150,
                  width: 400,
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: usernametexteditingcontroller,
                  decoration: InputDecoration(
                    hintText: "Enter Username",
                    label: const Text("Enter Username"),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    prefixIcon: const Icon(Icons.person),
                  ),
                  validator: (value) {
                    print("In Username Validator");
                    if (value == null || value.isEmpty) {
                      return "please enter username";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: passwordtexteditingcontroller,
                  obscureText: true,
                  obscuringCharacter: "*",
                  decoration: InputDecoration(
                      hintText: "Enter Password",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      prefixIcon: Icon(Icons.lock),
                      suffixIcon: Icon(Icons.remove_red_eye_outlined)),
                  validator: (value) {
                    print("In Password Validator");
                    if (value == null || value.isEmpty) {
                      return "Please Enter Password";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                    onPressed: () {
                      bool loginvalidated = _formkey.currentState!.validate();
                      if (loginvalidated) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Login Successful"),
                          ),
                        );
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(content: Text("Login Failed")));
                      }
                    },
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromARGB(255, 97, 95, 207)),
                    child: Text(
                      "Login",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontStyle: FontStyle.italic),
                    ))
              ],
            ),
          )),
    );
  }
}
